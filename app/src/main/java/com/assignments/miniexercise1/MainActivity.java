package com.assignments.miniexercise1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void openScreen1(View view) {
        Intent intent = new Intent(MainActivity.this, Screen1.class);
        startActivity(intent);
    }

    public void openScreen2(View view) {
        Intent intent = new Intent(MainActivity.this, Screen2.class);
        startActivity(intent);
    }

    public void openScreen3(View view) {
        Intent intent = new Intent(MainActivity.this, Screen3.class);
        startActivity(intent);
    }
}
