package com.assignments.miniexercise1;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Screen1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen1);
    }

    public void close(View view) {
        finish();
        //Intent intent = new Intent(Screen1.this, MainActivity.class);
        //startActivity(intent);
    }
}
